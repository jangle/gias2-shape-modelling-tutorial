.. GIAS2 Shape Modelling documentation master file, created by
   sphinx-quickstart on Sun Jul  9 17:46:57 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GIAS2 Shape Modelling
=====================

*Version* |version|

This is a tutorial for using the `GIAS2 <https://bitbucket.org/jangle/gias2>`_ package for creating and using statistical shape models.

To get started, clone or download and extract the `tutorial Git repository <https://bitbucket.org/jangle/gias2-shape-modelling-tutorial>`_ then follow the instructions below. 

If you already know or don't care what GIAS2 is, feel free to skip the first section. If you already have GIAS2 0.4.13 or higher installed, then feel free jump straight to the Shape Modelling Workflow section.

Contents
========

.. toctree::
   :maxdepth: 1
   :titlesonly:

   gias2
   installation
   shape-modelling-workflow

Citation
========
If you have found this tutorial useful, please cite our paper:

Ju Zhang, Jacqui Hislop-Jambrich, Thor F. Besier, `Predictive statistical models of baseline variations in 3-D femoral cortex morphology`, Medical Engineering & Physics, Volume 38, Issue 5, 2016, Pages 450-457, ISSN 1350-4533, `<http://dx.doi.org/10.1016/j.medengphy.2016.02.003>`_.

Copyright
=========

This documentation is part of GIAS2.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

Copyright Ju Zhang 2017