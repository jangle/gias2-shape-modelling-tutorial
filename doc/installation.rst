***********************
GIAS2 Installation
***********************

If you already have GIAS2 0.4.22 or higher installed, then feel free to skip this section.

Windows - Python 2 Anaconda Environment
=======================================

Anaconda is an umbrella package of Python packages for scientific computing. It is a convenient way to set up a Python environment with the dependencies GIAS2.

1. Install `Anaconda <https://www.continuum.io/downloads>`_ for Python 2.7
2. Launch the Anaconda Navigator and create a new virtual environment with Python 2.7 and launch a terminal for the environment.
3. Install GIAS2 dependencies. Enter the following in the terminal::
        
    conda install pyqt=4 mayavi matplotlib scipy

Enter "y" for questions confirming the install.  
    
4. Download the `latest GIAS2 release <https://bitbucket.org/jangle/gias2/downloads>`_ in the whl format. Use the latest Python 2 version, e.g. 0.4.22 or higher.
5. Install GIAS2 via the commandline prompt::

    pip install path\to\gias2-X.X.X-py2-none-any.whl

Change :code:`path\to` with the actual location of the file. X.X.X is the version number.

5. Set an environment variable to make sure Mayavi works for visualisation::
    
    set QT_API=pyqt
        
You may or may not need this, depending if you've installed Qt5 at some point. If you do need this command, you will need to run this command whenever you start a new terminal. See `here <https://conda.io/docs/using/envs.html#windows>`_ to set environment variables permanently.

6. You now should be able to import gias2 in a python session::
    
        python
        import gias2

Ubuntu 16.04 - Python 2 
=======================
1. Open a terminal, install dependencies::

    sudo apt-get install python-vtk scipy python-wxgtk3.0
    pip install --user mayavi matplotlib

2. Download the `latest GIAS2 release <https://bitbucket.org/jangle/gias2/downloads>`_ in the whl format. Use the latest Python 2 version, e.g. 0.4.22 or higher.
3. Install GIAS2 via the commandline prompt::

        pip install path\to\gias2-X.X.X-py2-none-any.whl

Change :code:`path\to` to the actual location of the file. X.X.X is the version number.

4. You now should be able to import gias2 in a python session::
    
    python
    import gias2

Python 3
========

Python 3 is not supported because Mayavi does not work in Python 3. However, you can follow the instructions above to install without Mayavi. Just leave out the :code:`-v` option when running scripts. 
    